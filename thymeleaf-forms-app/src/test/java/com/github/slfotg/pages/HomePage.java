package com.github.slfotg.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import net.thucydides.core.pages.PageObject;

public class HomePage extends PageObject {

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public String getHeaderText() {
        return find(By.tagName("h1")).getText();
    }

}
