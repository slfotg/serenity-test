package com.github.slfotg;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import com.github.slfotg.steps.HomePageSteps;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;

@RunWith(SerenityRunner.class)
public class HomePageIT {

    @Managed
    WebDriver webDriver;

    @Steps
    HomePageSteps user;

    @Test
    public void homePageHeaderShouldBeHi() {
        // Given
        user.onHomePage();
        // Then
        user.headerIs("Hi");
    }

}
