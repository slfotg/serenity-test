package com.github.slfotg.steps;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import com.github.slfotg.pages.HomePage;

import net.thucydides.core.annotations.Step;

public class HomePageSteps {

    String actor;

    HomePage homePage;

    @Step("Given #actor on homepage")
    public void onHomePage() {
        homePage.open();
    }

    @Step("Header should be {1}")
    public void headerIs(String title) {
        assertThat(homePage.getHeaderText(), equalTo(title));
    }
}
